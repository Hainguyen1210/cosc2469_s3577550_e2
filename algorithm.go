package main

import (
	"fmt"
	"time"
)

type Timer struct {
	results [10] int64
}
func (timer Timer)timer_get_aver()(int64){
	sum := int64(0)
	counter := int64(0)
	for value := range timer.results {
		counter ++
		sum += timer.results[value]
	}
	return sum/counter
}

func linear_ite_exponentiate(x float64, n uint64) (float64){
	if n == 0 {
		return 0
	}else {
		result := x
		for ; n > 1 ; n-- {
			result *= x
		}
		return result
	}
}

func linear_rec_exponentiate(x float64, n uint64) (result float64)  {
	if n == 0 {
		return 1
	} else if n == 1 {
		return x
	}else {
		return x * linear_rec_exponentiate(x, n-1)
	}
}

func log_ite_exponentiate(x float64, n uint64)( result float64)  {
	result = 1;
	for ;n != 0;  {
		if n%2 == 0 {
			x *= x
			n /= 2
		} else {
			n -= 1
			result *= x
		}
	}
	return result
}

func log_rec_exponentiate(x float64, n uint64) (float64){
	if n == 0 {
		return 1
	}
	if n % 2 == 0 {
		x = log_rec_exponentiate(x, n/2)
		return x*x
	} else {
		oldX := x
		x = log_rec_exponentiate(x, n/2)
		return x * x * oldX
	}
}

/*
COST CALCULATION
T(n)= T(n-1) + T(n-2)
	= 2T(n-1)  (upper bound => T(n-2) = T(n-1) )
	= 4T(n-2)
	= 8T(n-3)
	...
	= (2^n)T(n-n)
==> Exponential
 */
func recursive_fibonacci(n uint64) (float64) {
	if n <= 1{
		return float64(n)
	} else {
		return recursive_fibonacci(n-1) + recursive_fibonacci(n-2)
	}
}

/*
COST CALCULATION
no recursive call, 1 loop from 1 to n
==> Linear
 */
func iterative_fibonacci(n uint64) (float64) {
	smaller := float64(0)
	larger := float64(1)

	if n <= 1 {
		return float64(n)
	}
	for i := uint64(1); i < n; i++ {
		temp := larger
		larger += smaller
		smaller = temp
	}
	return larger
}

func test_exponential(x float64, max uint64)  {
	var timer Timer

	fmt.Println("linear_iterative, linear_recursive, logarithmic_iterative, logarithmic_recursive")
	for n := uint64(1); n < max; n *=10 {

		for i := range timer.results  {
			current_time := time.Now()
			linear_ite_exponentiate(x, n)
			timer.results[i] = time.Since(current_time).Nanoseconds()
		}
		fmt.Printf("%d,", timer.timer_get_aver())

		for i := range timer.results  {
			current_time := time.Now()
			linear_rec_exponentiate(x, n)
			timer.results[i] = time.Since(current_time).Nanoseconds()
		}
		fmt.Printf("%d,", timer.timer_get_aver())

		for i := range timer.results  {
			current_time := time.Now()
			log_ite_exponentiate(x, n)
			timer.results[i] = time.Since(current_time).Nanoseconds()
		}
		fmt.Printf("%d,", timer.timer_get_aver())

		for i := range timer.results  {
			current_time := time.Now()
			log_rec_exponentiate(x, n)
			timer.results[i] = time.Since(current_time).Nanoseconds()
		}
		fmt.Printf("%d\n", timer.timer_get_aver())
	}
}

func test_fibonacci(max uint64)  {

	var timer Timer
	fmt.Print("recursive, iterative")

	for n := uint64(1); n < max; n +=1 {
		for i := range timer.results  {
			current_time := time.Now()
			recursive_fibonacci(n)
			timer.results[i] = time.Since(current_time).Nanoseconds()
		}
		fmt.Printf("%d,", timer.timer_get_aver())

		for i := range timer.results  {
			current_time := time.Now()
			iterative_fibonacci(n)
			timer.results[i] = time.Since(current_time).Nanoseconds()
		}
		fmt.Printf("%d\n", timer.timer_get_aver())
	}
}

func main() {
	//X := float64(2)
	//exponent := uint64(20)
	//fmt.Println(linear_ite_exponentiate(X, exponent))
	//fmt.Println(linear_rec_exponentiate(X, exponent))
	//fmt.Println(log_ite_exponentiate(X, exponent))
	//fmt.Println(log_rec_exponentiate(X, exponent))

	// test_exponential(2, 100000000)
	//test_fibonacci(50)

	/*
	question 4: the empirical results follow the theory
	2 linear exponential functions does grows arcordingly to the
	 input and significantly slower than the 2 logarithmic functions
	
	2 fibonacci functions do have obvious different growth scale
	recursive function has exponential growth (2^n)
	and the other is linear.

	question 5: 
	 - linear functions calculating 2^n: the  recursive function is slower 
	 than the iterative one, it might because of the memory occipied by 
	 many recursive calls. same growth linear.
	 - logarithmic functions calculating 2^n: the recursive algorithm is also 
	 slower than the other one. same growth logarithmic.
	 - fibonacci functions: the recursive one significantly slower than the 
	 iterative function. not the same growth.
	*/
}

